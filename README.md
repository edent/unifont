# Unifont WOFF2

The [GNU Unifont](http://unifoundry.com/unifont/), converted to WOFF2 for use on websites.

Read [Terence Eden's blog post about Unifont](https://shkspr.mobi/blog/2019/04/banish-the-�-with-unifont/) for more details.
